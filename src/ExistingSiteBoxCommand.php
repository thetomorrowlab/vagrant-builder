<?php

namespace VagrantBoxBuilder;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExistingSiteBoxCommand extends BaseBoxCommand
{
    protected function configure()
    {
        $this->setName('ExistingSiteBox')
            ->setDescription('Builds you a box which you can use for existing code. It won\'t install any framework or CMS itself');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

    }

    protected function getReplacements() {
        $replacements = [];
        $scripts = [];
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/add-gitignore.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-git.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-composer.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/nginx-add-host.php", args: "'.$this->answers['hostname'].' /vagrant/site"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/xdebug/install-xdebug.sh"';
        $replacements['scripts'] = implode("\n", $scripts);
        return $replacements;
    }

}