<?php

namespace VagrantBoxBuilder;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SimpleSiteBox extends BaseBoxCommand
{
    protected function configure()
    {
        $this->setName('SimpleSiteBox')
            ->setDescription('Builds you a box for a simple slim site');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

    }

    protected function getReplacements() {
        $replacements = [];
        $scripts = [];
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/add-gitignore.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-git.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-composer.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/nginx-add-host.php", args: "'.$this->answers['hostname'].' /vagrant/site/public"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-single-site.sh", args: "'.$this->answers['hostname'].'" ';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/xdebug/install-xdebug.sh"';
        $replacements['scripts'] = implode("\n", $scripts);
        return $replacements;
    }

}