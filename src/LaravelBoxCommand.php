<?php
namespace VagrantBoxBuilder;

class LaravelBoxCommand extends BaseBoxCommand
{

    protected function configure()
    {
        $this->setName('LaravelBox')
            ->setDescription('Builds your base box, prepares hosts file for Laravel');
    }


    protected function getReplacements() {
        $replacements = [];
        $scripts = [];
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/add-gitignore.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-composer.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/nginx-add-host.php", args: "'.$this->answers['hostname'].' /vagrant/site/public"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-laravel.sh", args: "'.$this->answers['hostname'].'"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/laravel/foundation/setup-foundation-with-laravel-elixir.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/xdebug/install-xdebug.sh"';
        $replacements['scripts'] = implode("\n", $scripts);
        return $replacements;
    }
}
