#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install software-properties-common python-software-properties -y
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get install php7.1 -y
sudo apt-get install php7.1-cgi -y
sudo apt-get install php7.1-cli -y
sudo apt-get install php7.1-fpm -y
sudo apt-get install php7.1-dev -y
sudo apt-get install php7.1-curl -y
sudo apt-get install php7.1-gd -y
sudo apt-get install php7.1-intl -y
sudo apt-get install php7.1-mcrypt -y
sudo apt-get install php7.1-readline -y
sudo apt-get install php7.1-odbc -y
sudo apt-get install php7.1-recode -y
sudo apt-get install php7.1-xmlrpc -y
sudo apt-get install php7.1-xsl -y
sudo apt-get install php7.1-json -y
sudo apt-get install php7.1-sqlite3 -y
sudo apt-get install php7.1-mysql -y
sudo apt-get install php7.1-opcache -y
sudo apt-get install php7.1-bz2 -y
sudo apt-get install php7.1-bcmath -y
sudo apt-get install php7.1-mbstring -y
sudo apt-get install php7.1-soap -y
sudo apt-get install php7.1-xml -y
sudo apt-get install php7.1-zip -y