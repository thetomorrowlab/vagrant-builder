#!/bin/bash


apt install build-essential libsqlite3-dev ruby-dev -y

# Install the gem
gem install mailcatcher

# Make it start on boot
echo "@reboot root $(which mailcatcher) --ip=0.0.0.0" >> /etc/crontab
update-rc.d cron defaults

# Make php use it to send mail
echo "sendmail_path = /usr/bin/env $(which catchmail) -f 'www-data@localhost'" >> /etc/php/7.1/mods-available/mailcatcher.ini

#try to launch it immediately.
root $(which mailcatcher) --ip=0.0.0.0 || true

# Notify php mod manager (5.5+)
# older ubuntus
#php5enmod mailcatcher
# xenial
phpenmod mailcatcher
