#!/usr/bin/env bash

if [ -f /dev-certs/${1}.cert ];
then
    echo "Certificate already made"
else
    echo "Making Certificate"
    cd /dev-certs
    echo -e "*" >> .gitignore
    openssl genrsa -out ${1}.key 2048
    openssl req -new -x509 -key ${1}.key -out ${1}.cert -days 3650 -subj /CN=${1}
fi