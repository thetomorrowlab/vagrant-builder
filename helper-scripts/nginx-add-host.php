#!/usr/bin/php
<?php


$hostsFileContents = <<<'EOD'

server {
       listen         80;
       server_name    {{HOSTNAME}};
       return         301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2;
    server_name {{HOSTNAME}};
    root {{ROOT}};
    ssl_certificate /dev-certs/{{HOSTNAME}}.cert;
    ssl_certificate_key /dev-certs/{{HOSTNAME}}.key;

    index index.html index.htm index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/myapp-error.log error;

    sendfile off;

    client_max_body_size 100m;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php7.1-fpm-{{HOSTNAME}}.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
    }

    location ~ /\.ht {
        deny all;
    }
}
EOD;


$phpPoolFileContents = <<<'POOL'

[{{HOSTNAME}}]
user = ubuntu
group = ubuntu
listen = /var/run/php7.1-fpm-{{HOSTNAME}}.sock
listen.owner = www-data
listen.group = www-data
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 3
chdir = /

POOL;


$hostsFileContents = str_replace('{{HOSTNAME}}', $argv[1], $hostsFileContents);
$hostsFileContents = str_replace('{{ROOT}}', $argv[2], $hostsFileContents);

file_put_contents('/etc/nginx/sites-available/'.$argv[1], $hostsFileContents);
$cmd = 'sudo ln -sf /etc/nginx/sites-available/'.$argv[1].' /etc/nginx/sites-enabled/'.$argv[1];


$phpPoolFileContents = str_replace('{{HOSTNAME}}', $argv[1], $phpPoolFileContents);
file_put_contents('/etc/php/7.1/fpm/pool.d/'.$argv[1].'.conf', $phpPoolFileContents);
passthru ( $cmd );
passthru('sudo service php7.1-fpm reload');
passthru('sudo service nginx reload');
