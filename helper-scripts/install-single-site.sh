#!/usr/bin/env bash
if [[ ! -f "/vagrant/site/composer.json" ]]; then
    sudo apt-get install pwgen -y
    cd /vagrant
    mkdir site
    git clone --depth 1 https://katiemcgowan@bitbucket.org/katiemcgowan/single-site-box.git site
    rm site/.git -R
    cd /vagrant/site
    composer install
fi;
