#!/usr/bin/env bash

if [[ ! -f "/vagrant/site/.env" ]]; then

    sed "s/{{DOMAIN}}/$1/g"  /vagrant/helper-scripts/wordpress.env > /vagrant/site/.env

    AUTH_KEY="$(openssl rand -hex 32)";
    SECURE_AUTH_KEY="$(openssl rand -hex 32)";
    LOGGED_IN_KEY="$(openssl rand -hex 32)";
    NONCE_KEY="$(openssl rand -hex 32)";
    NONCE_SALT="$(openssl rand -hex 32)";
    AUTH_SALT="$(openssl rand -hex 32)";
    SECURE_AUTH_SALT="$(openssl rand -hex 32)";
    LOGGED_IN_SALT="$(openssl rand -hex 32)";

    sed -i "s/{{AUTH_KEY}}/$AUTH_KEY/g" /vagrant/site/.env
    sed -i "s/{{SECURE_AUTH_KEY}}/$SECURE_AUTH_KEY/g" /vagrant/site/.env
    sed -i "s/{{LOGGED_IN_KEY}}/$LOGGED_IN_KEY/g" /vagrant/site/.env
    sed -i "s/{{NONCE_KEY}}/$NONCE_KEY/g" /vagrant/site/.env
    sed -i "s/{{AUTH_SALT}}/$AUTH_SALT/g" /vagrant/site/.env
    sed -i "s/{{SECURE_AUTH_SALT}}/$SECURE_AUTH_SALT/g" /vagrant/site/.env
    sed -i "s/{{LOGGED_IN_SALT}}/$LOGGED_IN_SALT/g" /vagrant/site/.env
    sed -i "s/{{NONCE_SALT}}/$NONCE_SALT/g" /vagrant/site/.env

fi;
