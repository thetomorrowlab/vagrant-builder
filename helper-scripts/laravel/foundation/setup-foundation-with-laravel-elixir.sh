#!/usr/bin/env bash

if [ ! -f "/vagrant/site/assets/js/start_foundation.js" ]
then
    cp /vagrant/helper-scripts/laravel/foundation/app.scss /vagrant/site/resources/assets/sass/app.scss
    cp /vagrant/helper-scripts/laravel/foundation/gulpfile.js /vagrant/site/gulpfile.js

    mkdir -p /vagrant/site/resources/assets/js/
    cp /vagrant/helper-scripts/laravel/foundation/start_foundation.js /vagrant/site/resources/assets/js/start_foundation.js
    cp /vagrant/helper-scripts/laravel/foundation/package.json /vagrant/site/package.json
fi;