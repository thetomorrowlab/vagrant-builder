#!/usr/bin/env bash

echo "Installing MariaDB"
sudo apt-get install software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://lon1.mirrors.digitalocean.com/mariadb/repo/10.1/ubuntu xenial main'
sudo apt-get update -y

echo "SETTING UP PREFS"
debconf-set-selections <<< "maria-db-10.1 mysql-server/root_password password root"
debconf-set-selections <<< "maria-db-10.1 mysql-server/root_password_again password root"

sudo apt-get install -qq mariadb-server

mysql -u root -proot -Bse "CREATE DATABASE IF NOT EXISTS site"
