#!/bin/bash

echo "stopping php and apache"
sudo service php7.1-fpm stop
sudo /etc/init.d/apache2 stop

if [ -f "/usr/lib/php/20160303/xdebug.so" ]
then
    echo "Xdebug already downloaded"
else
    echo "Downloading Xdebug"
    sudo apt-get install php7.1-dev -y
    cd /var
    sudo wget --tries=10 -nv  http://xdebug.org/files/xdebug-2.5.0.tgz
    sudo tar -xvzf xdebug-2.5.0.tgz
    cd /var/xdebug-2.5.0
    sudo phpize
    sudo ./configure
    sudo make
    sudo cp modules/xdebug.so /usr/lib/php/20160303/xdebug.so
fi

if [ -f "/usr/lib/php/20160303/xdebug.so" ]
then
    echo "Xdebug downloaded and unpacked successfully"
    echo ""
    echo "==== Xdebug has been installed successfully ===="
    echo "To change xdebug settings"
    echo "N.B. You may need to open a new shell session before using these"
    echo "(e.g. by typing vagrant ssh from a shell inside the particular vagrant directory) "
    echo "xdebug-off        - will switch off Xdebug"
    echo "xdebug-on         - will switch on Xdebug"
    echo "xdebug-autostart  - will switch on Xdebug and set it to start automatically"
    sudo cp /vagrant/helper-scripts/xdebug/xdebug-on-off.sh /etc/profile.d/xdebug.sh
    echo ""

else
    echo ""
    echo "============== ERROR =============="
    echo "Xdebug has either not been downloaded or unpacked successfully"
    echo "please check that this VM can access the internet then try again"
    echo "============= /ERROR =============="
    echo ""
    exit 1
fi

    echo "starting php and apache"
    sudo service php7.1-fpm start

