#!/usr/bin/env bash

sudo service apache2 stop
sudo apt-get remove --purge apache2 apache2-utils -y
sudo apt-get autoremove -y
sudo apt-get autoclean -y
sudo apt-get update
sudo apt-get install nginx -y
sudo update-rc.d nginx enable
sudo systemctl start nginx
